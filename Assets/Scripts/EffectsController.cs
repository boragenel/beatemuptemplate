﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectsController : MonoBehaviour {


    private static EffectsController _instance;

    public ParticleSystem bloodExplode;
    public ParticleSystem hitImpact1;


    static public EffectsController Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = UnityEngine.Object.FindObjectOfType(typeof(EffectsController)) as EffectsController;

                if (_instance == null)
                {
                    GameObject go = new GameObject("EffectsController");
                    DontDestroyOnLoad(go);
                    _instance = go.AddComponent<EffectsController>();
                }
            }
            return _instance;
        }
    }


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
