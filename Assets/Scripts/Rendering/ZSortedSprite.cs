﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//script to set an objects z index based on it's y position.
[ExecuteInEditMode]
public class ZSortedSprite : MonoBehaviour {

    public int order;

    SpriteRenderer sRenderer;
    //the max amount to sort by
    private int sortMargin = 1000;
    public bool liveUpdate = false;
    float prevPosY = 0;

    public bool manageChildrenZ = false;
    ZSortedChild[] childArray;

    public bool bDebug = false;

    private void Awake()
    {
        

        childArray = GetComponentsInChildren<ZSortedChild>();

        sRenderer = GetComponent<SpriteRenderer>();
    }

    void Start () {      
        //updateZSortingAccordingtoYPos(null);
     

    }

    //sets the sorting order based on the y position on the screen
    public void updateZSortingAccordingtoZPos()
    {
        
        //Vector2 vect2 = new Vector2(transform.position.x, transform.position.z);

        //float dist = Vector2.Distance(vect1, vect2);// - (transform.position.y*transform.localScale.y);
        //Quaternion rotation = Quaternion.FromToRotation(Vector3.forward, Vector3.right);
        //Vector3 rotPos = rotation * transform.position;


        order = Mathf.RoundToInt(transform.position.z * -100);

        if (manageChildrenZ)
        {
            
            foreach (ZSortedChild sortedChild in childArray)
            {
                sortedChild.rend.sortingOrder = order + 1 + sortedChild.childIndex;

            }

        } else if(sRenderer != null)
        {
            sRenderer.sortingOrder = order;
            
        }
        

    }


	// Update is called once per frame
	void Update () {

        //if game isnt playing, don't try update? Was this due to issues trying to update when it shouldn't be? DPJ
        if (!Application.isPlaying)
        {
            liveUpdate = true;
        }

        if (liveUpdate)
            updateZSortingAccordingtoZPos();
    }

}
