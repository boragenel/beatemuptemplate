﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Anima2D;

[ExecuteInEditMode]
public class ZSortedChild : MonoBehaviour {

    public int childIndex;
    public SpriteMeshInstance rend;

	// Use this for initialization
	void Start () {
        rend = GetComponent<SpriteMeshInstance>();

	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
