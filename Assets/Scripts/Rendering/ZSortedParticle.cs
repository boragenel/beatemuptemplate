﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//script to set an objects z index based on it's y position.
[ExecuteInEditMode]
public class ZSortedParticle : MonoBehaviour {

    GameObject distObj;

    public int order;

    ParticleSystemRenderer sRenderer;
    //the max amount to sort by
    private int sortMargin = 1000;
    public bool liveUpdate = false;
    float prevPosY = 0;

    public bool manageChildrenZ = false;
    ZSortedChild[] childArray;

    private void Awake()
    {


        childArray = GetComponentsInChildren<ZSortedChild>();

        sRenderer = GetComponent<ParticleSystemRenderer>();
    }

    void Start () {      
        //updateZSortingAccordingtoYPos(null);
     

    }

    //sets the sorting order based on the y position on the screen
    public void updateZSortingAccordingtoYPos()
    {

        order = Mathf.RoundToInt(transform.position.z * -100);

        sRenderer.sortingOrder = order;        

    }


	// Update is called once per frame
	void Update () {

        //if game isnt playing, don't try update? Was this due to issues trying to update when it shouldn't be? DPJ
        if (!Application.isPlaying)
        {
            liveUpdate = true;
        }

        if (liveUpdate)
            updateZSortingAccordingtoYPos();
    }

}
