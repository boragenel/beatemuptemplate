﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ProjectileType
{
    FIRE,
    ICE,
    DARK
}

public class ProjectileManager : MonoBehaviour {

    public ObjectPooler fireProjectiles;
    static ProjectileManager _instance;


    static public ProjectileManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = UnityEngine.Object.FindObjectOfType(typeof(ProjectileManager)) as ProjectileManager;

                if (_instance == null)
                {
                    GameObject go = new GameObject("ProjectileManager");
                    DontDestroyOnLoad(go);
                    _instance = go.AddComponent<ProjectileManager>();
                }
            }
            return _instance;
        }
    }



    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public GameObject getProjectile(ProjectileType _type)
    {
        switch(_type)
        {
            case ProjectileType.FIRE:
                return fireProjectiles.GetNextPooledObject();
            default:
                return null;
        }
        
    }

}
