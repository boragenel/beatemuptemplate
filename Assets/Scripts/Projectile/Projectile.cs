﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

    public float speed = 0.5f;
    public Vector3 direction;
    public bool homing = false;
    public GameObject target;
    public float lifeTime = 1f;
    public float LIFETIME_MAX = 1f;
    public float knockbackPower = 0.5f;

    // Use this for initialization
    void Start () {
		
	}
	
    public void Fire()
    {
        lifeTime = LIFETIME_MAX;
        if (direction.x < 0)
            transform.localScale = new Vector3(-1, 1, 1);
        else
            transform.localScale = new Vector3(1, 1, 1);

    }

    // Update is called once per frame
    void Update () {

        lifeTime -= Time.deltaTime;

        if( homing )
        {
            transform.position += (target.transform.position - transform.position).normalized * speed * Time.deltaTime;


        } else
        {
            transform.position += direction * speed * Time.deltaTime;
        }

        if (lifeTime < 0)
            gameObject.SetActive(false);

	}
}
