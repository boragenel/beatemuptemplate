﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public enum BlendMode
{
    Opaque,
    Cutout,
    Fade,        // Old school alpha-blending mode, fresnel does not affect amount of transparency
    Transparent // Physically plausible transparency mode, implemented as alpha pre-multiply
}

public class HideObjects : MonoBehaviour
{

    public Transform WatchTarget;
    public LayerMask OccluderMask;

    private List<Transform> _LastTransforms;

    public float hideTime = 0.05f;

    void Start()
    {
        _LastTransforms = new List<Transform>();
    }

    void Update()
    {
        hideTime -= Time.deltaTime;
        if (hideTime > 0)
            return;
        

        MeshRenderer m;
        //reset and clear all the previous objects

        if (_LastTransforms.Count > 0)
        {
            foreach (Transform t in _LastTransforms)
            {

                m = t.GetComponent<MeshRenderer>();
                m.material.SetFloat("_Mode", 0);
                t.gameObject.layer = 8;
                SetupMaterialWithBlendMode(m.material, BlendMode.Opaque);

            }


            _LastTransforms.Clear();
        }
        

        //Cast a ray from this object's transform the the watch target's transform.
        RaycastHit[] hits = Physics.RaycastAll(
            transform.position,
            WatchTarget.transform.position - transform.position - (transform.forward * 10),
            Vector3.Distance(WatchTarget.transform.position, transform.position),
            OccluderMask
        );

        //Loop through all overlapping objects and disable their mesh renderer
        if (hits.Length > 0)
        {
            foreach (RaycastHit hit in hits)
            {
                if (hit.collider.gameObject.transform != WatchTarget && hit.collider.transform.root != WatchTarget)
                {

                    hit.collider.gameObject.layer = 9;
                    m = hit.collider.gameObject.GetComponent<MeshRenderer>();
                    
                    if (m == null)
                        continue;
                    m.material.SetFloat("_Mode", 2);
                    SetupMaterialWithBlendMode(m.material,BlendMode.Fade);

                    _LastTransforms.Add(hit.collider.gameObject.transform);
                }
            }
        }

        hideTime = 0.05f;

    }


    

    public void SetupMaterialWithBlendMode(Material material, BlendMode blendMode)
    {
        switch (blendMode)
        {
            case BlendMode.Opaque:
                material.SetOverrideTag("RenderType", "");
                material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
                material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);
                material.SetInt("_ZWrite", 1);
                material.DisableKeyword("_ALPHATEST_ON");
                material.DisableKeyword("_ALPHABLEND_ON");
                material.DisableKeyword("_ALPHAPREMULTIPLY_ON");
                material.renderQueue = -1;
                break;
            case BlendMode.Cutout:
                material.SetOverrideTag("RenderType", "TransparentCutout");
                material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
                material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);
                material.SetInt("_ZWrite", 1);
                material.EnableKeyword("_ALPHATEST_ON");
                material.DisableKeyword("_ALPHABLEND_ON");
                material.DisableKeyword("_ALPHAPREMULTIPLY_ON");
                material.renderQueue = 2450;
                break;
            case BlendMode.Fade:
                material.SetOverrideTag("RenderType", "Transparent");
                material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
                material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
                material.SetInt("_ZWrite", 0);
                material.DisableKeyword("_ALPHATEST_ON");
                material.EnableKeyword("_ALPHABLEND_ON");
                material.DisableKeyword("_ALPHAPREMULTIPLY_ON");
                material.renderQueue = 3000;
                break;
            case BlendMode.Transparent:
                material.SetOverrideTag("RenderType", "Transparent");
                material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
                material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
                material.SetInt("_ZWrite", 0);
                material.DisableKeyword("_ALPHATEST_ON");
                material.DisableKeyword("_ALPHABLEND_ON");
                material.EnableKeyword("_ALPHAPREMULTIPLY_ON");
                material.renderQueue = 3000;
                break;
        }
    }


}