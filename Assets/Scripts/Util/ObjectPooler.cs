﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

public class ObjectPooler : MonoBehaviour
{
    public List<GameObject> prefabsToPool;
    public int pooledAmount;

    private List<GameObject> pooledList;


    void Start()
    {
        pooledList = new List<GameObject>();
        foreach (GameObject prefab in prefabsToPool)
        {
            for (int i = 0; i < pooledAmount; i++)
            {
                AddToPoolList(prefab);
            }
        }
    }

    private GameObject AddToPoolList(GameObject prefab)
    {
        GameObject theGameObject = Instantiate(prefab, gameObject.transform);
        theGameObject.SetActive(false);
        pooledList.Add(theGameObject);
        return theGameObject;
    }

    private GameObject AddToPoolList(GameObject prefab, bool noParent)
    {
        GameObject theGameObject = Instantiate(prefab);
        theGameObject.SetActive(false);
        pooledList.Add(theGameObject);
        return theGameObject;
    }

    public GameObject GetRandomPooledObject()
    {

        int randomIndex = UnityEngine.Random.Range(0, prefabsToPool.Count);
        GameObject randomComponent = prefabsToPool[randomIndex];
        GameObject randomObject = Get(obj => obj.name.Contains(randomComponent.name));

        if (randomObject.GetComponent<SpriteRenderer>() != null)
            randomObject.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);

        return randomObject;
    }

    public GameObject GetNextPooledObject()
    {
        GameObject objRet = null;

        for(int i=0; i < pooledList.Count; i++)
        {
            if (!pooledList[i].activeSelf)
                objRet = pooledList[i];
        }

        if (objRet != null)
            objRet.SetActive(true);

        return objRet;
    }

    public GameObject Get(Func<GameObject, bool> predicate)
    {
        //this should give a list of all the inactive objects within the pooled list
        List<GameObject> inactiveObjects = pooledList.Where(go => go.activeInHierarchy == false).ToList();
        //Finds one and returns it, or return null if none are in the list (default)
        GameObject needle = inactiveObjects.FirstOrDefault(predicate);
        //If all of the objects of the search criteria we're looking for are active
        if (needle == null)
        {
            //check if the object we are looking for exists in an active states, meaning this pool is meant to hold such an object
            GameObject prefab = prefabsToPool.FirstOrDefault(predicate);
            //if you find a prefab that matches the search query
            if (prefab != null)
            {
                //we need to make a new instance of this prefab as all pooled ones are currently in use
                return AddToPoolList(prefab);
            }
            else
            {
                //else you're trying to get an item that isn't pooled in this pool
                throw new Exception("You've tried getting an unpooled object from the pool: " + predicate.ToString());
            }
        }
        else
        {
            //there is an inactive object that meets the search critera available, return it.
            return needle;
        }
    }

}