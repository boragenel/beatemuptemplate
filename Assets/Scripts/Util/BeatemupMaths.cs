﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeatemupMaths  {

    public static float zDifference = 1f;

    public static float GetFakeDistanceBetween(Vector3 pos1,Vector3 pos2)
    {

        return Mathf.Sqrt((Mathf.Pow((pos2.z - pos1.z) , 2)/ zDifference) + Mathf.Pow((pos2.x - pos1.x), 2));

    }

    public static Vector3 GetFakeVectorBetween(Vector3 pos1,Vector3 pos2)
    {
        Vector3 v = pos1 - pos2;
        v.z /= zDifference;
        return v;
    }

}
