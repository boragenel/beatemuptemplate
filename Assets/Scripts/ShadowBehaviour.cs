﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShadowBehaviour : MonoBehaviour {

    RaycastHit hit;
    Vector3 baseScale;

    // Use this for initialization
    void Start () {
        baseScale = transform.localScale;
	}
	
	// Update is called once per frame
	void Update () {


        Physics.Raycast(transform.parent.position, Vector3.down, out hit, 10f,~LayerMask.GetMask("Player"));

        if( hit.collider )
        {
            transform.position = hit.point + (Vector3.up * 0.05f);
            
            transform.localScale = (1 -((transform.parent.position.y - hit.point.y) / 5f)) * baseScale;
        }

	}
}
