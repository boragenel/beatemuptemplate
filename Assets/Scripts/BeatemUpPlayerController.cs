﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BeatemUpPlayerController : MonoBehaviour {


    Rigidbody rbody;
    public float speed = 10f;
    public float jumpPower = 50f;

    public float velocityY = 0;
    public Vector3 knockbackDirection = Vector3.zero;

    public float fakeGravityPower = 10f;

    public bool isGrounded = false;

    private Animator animator;

    private int curDirection = 1;


    public float unconciousTimer = -1;

    [Header("Combos")]
    public string[] combo1TriggerNames;
    public int currentCombo1Indice = 0;
    public float comboTimer = 0;
    public float combo1Expire = 0.3f;

    [Header("Offense")]
    public float attackDuration = 1f;
    public float attackTimer = 0;
    public float knockbackPower = 4f;

    public bool queuedPunch = false;

    [Header("Grapple")]
    public EnemyAI grappledEnemy = null;
    private int grappleHits = 0;

    public GameObject damageSphere;

    LayerMask groundMask;

    Vector3 direction;

	// Use this for initialization
	void Start () {

        rbody = GetComponent<Rigidbody>();
        groundMask = LayerMask.NameToLayer("Ground");
        animator = GetComponent<Animator>();

        

    }
	
	// Update is called once per frame
	void Update () {

        CheckGrounded();
        HandleInput();
        ApplyFakeGravity();
        HandleKnockback();
        HandleTimers();
        HandleCombos();
    }

    private void HandleCombos()
    {

        if (comboTimer >= combo1Expire)
        {
            currentCombo1Indice = 0;
            comboTimer = -1;
        }

    }

    private void HandleTimers()
    {
        if(attackTimer > 0 )
        {
            
            attackTimer -= Time.deltaTime;
            if (attackTimer <= 0 && queuedPunch)
            {
                Punch();
                queuedPunch = false;
            }

        }

        if( comboTimer >= 0)
            comboTimer += Time.deltaTime;


        if (unconciousTimer > 0)
            unconciousTimer -= Time.deltaTime;



    }

    private void HandleKnockback()
    {

        

        if ( knockbackDirection.magnitude < 0.1f)
        {
            knockbackDirection = Vector3.zero;
        } else
        {
            knockbackDirection *= 0.9f;
        }

    }

    void Punch()
    {

        animator.SetBool("Walking", false);

        if(grappledEnemy)
        {
            attackTimer = attackDuration;
            animator.SetTrigger("Punch1");
            grappleHits++;
            if( grappleHits < 4)
                knockbackPower = 0f;
            else if(grappleHits == 4)
                knockbackPower = 4f;
            else
            {
                
                grappledEnemy.changeState(new TakeDamage(grappledEnemy));
                animator.SetBool("Grapple", false);
                grappledEnemy = null;

            }
        } else
        if (velocityY != 0) // IF IN THE AIR
        {
            attackTimer = attackDuration;
            animator.SetTrigger("JumpKick");
            knockbackPower = 4f;

        }
        else
        {



            attackTimer = attackDuration;
            animator.SetTrigger(combo1TriggerNames[currentCombo1Indice]);

            if (currentCombo1Indice == combo1TriggerNames.Length-1)
                knockbackPower = 1f;
            else knockbackPower = 0f;

            comboTimer = 0;
            currentCombo1Indice++;
            if (currentCombo1Indice == combo1TriggerNames.Length)
                currentCombo1Indice = 0;
        }

    }

    void HandleInput()
    {

        direction = new Vector3();
        direction.x = Input.GetAxis("Horizontal");
        direction.z = Input.GetAxis("Vertical"); // * 3f


        if (Input.GetKeyDown(KeyCode.J)  )
        {
            if(attackTimer <= 0)
                Punch();
            else queuedPunch = true;
        }
        
            




        // if we have any obstacles ahead, we will move slower, so we can jump or not push things
        if (AnyObstaclesAhead(new Vector3(direction.x,0,0) ) )
        {
            
            
            direction.x *= 0.05f;
        }
        if (AnyObstaclesAhead(new Vector3(0, 0, direction.z)))
        {
            

            direction.z *= 0.05f;
        }

        if (direction.x < 0)
            transform.localScale = new Vector3(-1, 1, 1);
        else if (direction.x > 0)
            transform.localScale = new Vector3(1, 1, 1);

        if ( (attackTimer <= 0 || (attackTimer > 0 && velocityY > 0  ) )  && !queuedPunch && !grappledEnemy)
        {

           

            if (direction.magnitude > 0)
                animator.SetBool("Walking", true);
            else
                animator.SetBool("Walking", false);

            if( direction.magnitude > 0 )
            { 
                comboTimer = -1;
                currentCombo1Indice = 0;
            }

        }
        else if( velocityY == 0) {
            direction = Vector3.zero;
        }

        Vector3 velo = direction * speed  * Time.deltaTime * 60f;
        velo.y = velocityY;

        rbody.velocity = velo + knockbackDirection;

        if( Input.GetKeyDown(KeyCode.Space) && velocityY == 0)
        {
            velocityY += jumpPower;
            
            
        }

        


    }

    void CheckGrounded()
    {



        RaycastHit hit;
        Physics.Raycast(transform.position - (Vector3.up*0.8f), -Vector3.up, out hit, 0.3f, LayerMask.GetMask("Ground","GroundT"));
        //Debug.DrawRay(transform.position - (Vector3.up * 0.8f), -Vector3.up*0.3f, Color.red, 0.3f);

        if( hit.collider )
        {
            isGrounded = true;
            if (velocityY < 0)
            {

                velocityY = 0;
            }

            

        } else
        {
            
            isGrounded = false;
            
        }

    }

    void ApplyFakeGravity()
    {

        if(!isGrounded)
        {
            velocityY -= fakeGravityPower *Time.deltaTime * 60f;
        }

    }


    bool AnyObstaclesAhead(Vector3 direction)
    {

        RaycastHit hit;
        Physics.Raycast(transform.position+(Vector3.down * 0.8f), direction, out hit, 0.7f, ~LayerMask.GetMask("Player"));
        Debug.DrawRay(transform.position + (Vector3.down * 0.8f), direction, Color.red, 0.7f);
        return hit.collider != null;
        


    }


    private void OnTriggerEnter(Collider other)
    {
        if( other.tag == "EnemyDamage")
        {

            // TODO: recoveryTime
            unconciousTimer = 1f;
            animator.SetTrigger("TakeDamage1");

            EnemyAI ai = other.transform.parent.GetComponent<EnemyAI>();
            // damage here

            EffectsController.Instance.hitImpact1.transform.position = other.transform.position;
            EffectsController.Instance.hitImpact1.Play();

            // knockback here
            if (knockbackDirection == Vector3.zero)
                knockbackDirection = ai.knockbackPower * (transform.position -  ai.transform.position ).normalized;

        } else if (other.tag == "EnemyProjectileDamage1")
        {

            // TODO: recoveryTime
            unconciousTimer = 1f;
            animator.SetTrigger("TakeDamage");


            Projectile _projectile = other.GetComponent<Projectile>();
            // damage here

            EffectsController.Instance.hitImpact1.transform.position = other.transform.position;
            EffectsController.Instance.hitImpact1.Play();

            // knockback here
            if (knockbackDirection == Vector3.zero)
                knockbackDirection = _projectile.knockbackPower * (transform.position - _projectile.transform.position).normalized;

            _projectile.gameObject.SetActive(false);

        }
        else if (other.tag == "Enemy")
        {
            if( !grappledEnemy && velocityY == 0 && direction.magnitude != 0 && attackTimer <= 0 && animator.GetBool("Walking") && !queuedPunch)
            { 
                animator.SetBool("Walking", false);
                animator.SetBool("Grapple",true);
                EnemyAI en = other.GetComponent<EnemyAI>();

                grappledEnemy = en;
                grappleHits = 0;

                en.changeState(new GetGrappled(en, damageSphere ));
            }





        }
    }

}
