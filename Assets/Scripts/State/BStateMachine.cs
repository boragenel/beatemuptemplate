﻿using UnityEngine;
/**
 * 
 * @author Bora Genel
 * @company Brainoid Games
 * @date 24.07.2011
 */
public class BStateMachine : MonoBehaviour
{
    public ActionBase currentState;
    public ActionType curStateName = ActionType.NONE;
    public ActionType lastState = ActionType.NONE;

    public BStateMachine()
    {
        currentState = null;
    }

    // Update the FSM. Parameter is the frametime for this frame.
    public void UpdateState()
    {
        if (currentState != null)
        {
            currentState.Update();
        }
    }



    // Change to another state
    public void changeState(ActionBase s)
    {



        if (currentState != null)
        {
            lastState = currentState.type;
            currentState.Exit();
        }


        currentState = s;
        curStateName = currentState.type;
        if (currentState != null)
        {
            currentState.Enter();
        }
    }
}