﻿using UnityEngine;
using System.Collections;

public interface BState
{
    void Enter(); // called on entering the state
    void Exit(); // called on leaving the state
    void Update();
    // called every frame while in the state
}