﻿using UnityEngine;
using System.Collections;

public class EnemyIdle : ActionBase
{

    private EnemyAI enemy;
    


    public EnemyIdle(EnemyAI _enemy)
    {
        type = ActionType.IDLE;
        enemy = _enemy;
        
    }

    public override void Destroy()
    {

        enemy = null;

    }

    public override void Enter()
    {
        enemy.agent.isStopped = true;
        enemy.animator.SetBool("Walking", false);



    }

    public override void Exit()
    {

        
        Destroy();
    }

    public override void Update()
    {
        
    }
}