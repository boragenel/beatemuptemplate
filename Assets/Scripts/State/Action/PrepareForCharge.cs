﻿using UnityEngine;
using System.Collections;

public class PrepareForCharge : ActionBase
{

    private EnemyAI enemy;
    private Transform target;
    Coroutine prepareRoutine;

    public PrepareForCharge(EnemyAI _enemy, Transform _target,float range)
    {
        type = ActionType.PREPARE_FOR_CHARGE;
        enemy = _enemy;
        target = _target;

    }

    IEnumerator GetReadyForCharge()
    {
        enemy.agent.isStopped = true;

        enemy.unconciousTimer = 3.5f;
        yield return new WaitForSeconds(0.8f);

        enemy.agent.isStopped = false;

       enemy.changeState(new BasicCharge(enemy,target));

    }


    public override void Destroy()
    {

        enemy = null;

    }

    public override void Enter()
    {
        enemy.agent.isStopped = true;
        enemy.animator.SetBool("Walking", false);
        enemy.curAttackingTime = enemy.attackTime;
        
        enemy.animator.SetTrigger("Punch1");
        prepareRoutine = enemy.StartCoroutine(GetReadyForCharge());


    }

    public override void Exit()
    {
        enemy.StopCoroutine(prepareRoutine);
        enemy.agent.isStopped = false;
        Destroy();
    }

    public override void Update()
    {
        if( enemy.curAttackingTime <= 0 && enemy.unconciousTimer <= 0)
        {
            enemy.changeState(new EnemyIdle(enemy));
        }
    }
}