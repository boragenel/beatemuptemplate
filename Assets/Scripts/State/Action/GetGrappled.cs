﻿using UnityEngine;
using System.Collections;

public class GetGrappled : ActionBase
{

    private EnemyAI enemy;

    private GameObject follow;

    public GetGrappled(EnemyAI _enemy,GameObject _follow)
    {
        type = ActionType.IDLE;
        enemy = _enemy;
        follow = _follow;
        
    }

    public override void Destroy()
    {

        enemy = null;

    }

    public override void Enter()
    {
        enemy.agent.isStopped = true;
        enemy.gotGrappled = true;
        enemy.animator.SetBool("Walking", false);
        enemy.animator.SetBool("Walking", false);
        enemy.animator.SetBool("GotGrappled", true);
        enemy.rbody.velocity = Vector3.zero;
        enemy.GetComponent<SphereCollider>().enabled = false;
        enemy.unconciousTimer = 5f;

        




    }

    public override void Exit()
    {
        enemy.GetComponent<SphereCollider>().enabled = true;

        enemy.gotGrappled = false;
        enemy.animator.SetBool("GotGrappled", false);
        enemy.agent.enabled = true;
        enemy.agent.isStopped = false;
        
        Destroy();
    }

    public override void Update()
    {
        enemy.transform.position = follow.transform.position;

        if (follow.transform.parent.position.x < enemy.transform.position.x)
        {
            enemy.transform.localScale = new Vector3(-1, 1, 1);
        }
        else enemy.transform.localScale = new Vector3(1, 1, 1);

    }
}