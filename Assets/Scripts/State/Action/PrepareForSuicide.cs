﻿using UnityEngine;
using System.Collections;


public class PrepareForSuicide : ActionBase
{

    private EnemyAI enemy;
    private Transform target;
    Coroutine prepareRoutine;

    public PrepareForSuicide(EnemyAI _enemy, Transform _target)
    {
        type = ActionType.PREPARE_FOR_CHARGE;
        enemy = _enemy;
        target = _target;

    }

    IEnumerator GetReadyForSuicide()
    {

        enemy.agent.isStopped = true;

        enemy.unconciousTimer = 3.5f;

        int i = 0;
        while( i < 10)
        {

            enemy.transform.localScale *= 1.03f;
            i++;
            yield return new WaitForSeconds(0.05f);
        }

        enemy.agent.isStopped = false;


        
        enemy.StopCoroutine(prepareRoutine);


        EffectsController.Instance.bloodExplode.transform.position = enemy.transform.position + enemy.projectileSpawnOffset;
        EffectsController.Instance.bloodExplode.Play();

        enemy.gameObject.SetActive(false);



    }


    public override void Destroy()
    {

        enemy = null;
        prepareRoutine = null;

    }

    public override void Enter()
    {
        enemy.agent.isStopped = true;
        enemy.animator.SetBool("Walking", false);
        
        
        
        prepareRoutine = enemy.StartCoroutine(GetReadyForSuicide());


    }

    public override void Exit()
    {
        
        Destroy();
    }

    public override void Update()
    {
        if( enemy.curAttackingTime <= 0 && enemy.unconciousTimer <= 0)
        {
            enemy.changeState(new EnemyIdle(enemy));
        }
    }
}