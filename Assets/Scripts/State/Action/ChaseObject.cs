﻿using UnityEngine;
using System.Collections;

public class ChaseObject : ActionBase
{

    private EnemyAI enemy;
    private Transform target;

    public ChaseObject(EnemyAI _enemy,Transform _target)
    {
        type = ActionType.CHASE_OBJECT;
        enemy = _enemy;
        target = _target;

    }

    public override void Destroy()
    {
        target = null;
        enemy = null;

    }

    public override void Enter()
    {
        enemy.agent.isStopped = false;
        enemy.animator.SetBool("Walking", true);

        


    }

    public override void Exit()
    {

        
        Destroy();
    }

    public override void Update()
    {

        if (!enemy.playerInAggroRadius)
        {
            enemy.changeState(new EnemyIdle(enemy));
            return;
        }
       


        if ( enemy.unconciousTimer > 0) {
            enemy.changeState(new EnemyIdle(enemy));
            return;
        }

        if (enemy.agent.destination.x < enemy.transform.position.x)
        {
            enemy.transform.localScale = new Vector3(-1, 1, 1);
        }
        else enemy.transform.localScale = new Vector3(1, 1, 1);



        switch (enemy.enemyStyle)
        {
            case EnemyStyle.CHASE:
                if (!enemy.playerInAttackRadius)
                {
                    enemy.GoTo(target.position);
                }
                break;
            case EnemyStyle.GUARD:
                if (!enemy.playerInAttackRadius)
                {
                    if (BeatemupMaths.GetFakeDistanceBetween(enemy.transform.position, enemy.guardingPos) < enemy.guardingRadius)
                    {
                        enemy.GoTo(target.transform.position);

                    }
                    else
                    {
                        enemy.changeState(new Guard(enemy));

                    }
                }
                break;
            case EnemyStyle.KEEPDISTANCE:
                if (BeatemupMaths.GetFakeDistanceBetween(target.position, enemy.transform.position) < enemy.attackRange * 0.9f)
                {
                    
                    enemy.changeState(new KeepDistance(enemy, target));

                }
                else
                {

                    enemy.GoTo(target.transform.position);

                }
                break;
            case EnemyStyle.GUARD_KEEPDISTANCE:

                if (BeatemupMaths.GetFakeDistanceBetween(enemy.transform.position, enemy.guardingPos) > enemy.guardingRadius)
                {

                    enemy.changeState(new Guard(enemy));


                }
                else if (BeatemupMaths.GetFakeDistanceBetween(target.position, enemy.transform.position) < enemy.attackRange * 0.9f)
                {
                    enemy.changeState(new KeepDistance(enemy, target));
                }
                else
                {
                    enemy.GoTo(target.transform.position);
                }
                break;
        }


    }
}