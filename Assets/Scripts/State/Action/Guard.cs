﻿using UnityEngine;
using System.Collections;

public class Guard : ActionBase
{

    private EnemyAI enemy;

    public Guard(EnemyAI _enemy)
    {
        type = ActionType.GUARD;
        enemy = _enemy;

    }

    public override void Destroy()
    {
        
        enemy = null;

    }

    public override void Enter()
    {
        enemy.agent.isStopped = false;
        enemy.animator.SetBool("Walking", true);        

    }

    public override void Exit()
    {

        
        Destroy();
    }

    public override void Update()
    {
        

        if (enemy.curAttackingTime > 0 || enemy.unconciousTimer > 0)
            return;

        if (enemy.agent.destination.x < enemy.transform.position.x)
        {
            enemy.transform.localScale = new Vector3(-1, 1, 1);
        }
        else enemy.transform.localScale = new Vector3(1, 1, 1);

        enemy.GoTo(enemy.guardingPos, false);


        if ( BeatemupMaths.GetFakeDistanceBetween(enemy.transform.position,enemy.agent.destination) < 1.4f)
        {
            enemy.changeState(new EnemyIdle(enemy));
        }


    }
}