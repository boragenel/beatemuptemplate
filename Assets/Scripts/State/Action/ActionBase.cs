﻿using UnityEngine;
using System.Collections;

public enum ActionType
{
    NONE,
    IDLE,
    CHASE_OBJECT,
    KEEP_DISTANCE,
    GUARD,
    BASIC_MELEE_ATTACK,
    BASIC_RANGED_ATTACK,
    PREPARE_FOR_CHARGE,
    BASIC_CHARGE,
    RECOVER_FROM_CHARGE
    
}

public class ActionBase : BState
{

    public ActionType type = ActionType.IDLE;
    public string generalType = "";


    public ActionBase()
    {
    }

    public virtual void Enter()
    {



    }

    public virtual void Update()
    {

    }

    public virtual void Exit()
    {

    }

    public virtual void Destroy()
    {

    }
}