﻿using UnityEngine;
using System.Collections;

public class RecoverFromCharge : ActionBase
{

    private EnemyAI enemy;
    


    public RecoverFromCharge(EnemyAI _enemy)
    {
        type = ActionType.RECOVER_FROM_CHARGE;
        enemy = _enemy;
        
    }

    public override void Destroy()
    {

        enemy = null;

    }

    public override void Enter()
    {
        enemy.agent.isStopped = true;
        enemy.animator.SetBool("Walking", false);
        enemy.unconciousTimer = 0.8f;


    }

    public override void Exit()
    {

        enemy.agent.isStopped = false;
        Destroy();
    }

    public override void Update()
    {
        if (enemy.unconciousTimer <= 0)
            enemy.changeState(new EnemyIdle(enemy));
    }
}