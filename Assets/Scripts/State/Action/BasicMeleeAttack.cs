﻿using UnityEngine;
using System.Collections;

public class BasicMeleeAttack : ActionBase
{

    private EnemyAI enemy;
    private int runTime = 0;
    public BasicMeleeAttack(EnemyAI _enemy)
    {
        type = ActionType.BASIC_MELEE_ATTACK;
        enemy = _enemy;

    }

    public override void Destroy()
    {

        enemy = null;

    }

    public override void Enter()
    {
        enemy.agent.isStopped = true;
        enemy.animator.SetBool("Walking", false);
        enemy.curAttackingTime = enemy.attackTime;
        
        enemy.animator.SetTrigger("Punch1");


    }

    public override void Exit()
    {

        enemy.agent.isStopped = false;
        Destroy();
    }

    public override void Update()
    {
        if( enemy.curAttackingTime <= 0 && enemy.unconciousTimer <= 0)
        {
            enemy.changeState(new EnemyIdle(enemy));
        }
    }
}