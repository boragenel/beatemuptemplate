﻿using UnityEngine;
using System.Collections;

public class TakeDamage : ActionBase
{

    private EnemyAI enemy;
    


    public TakeDamage(EnemyAI _enemy)
    {
        type = ActionType.RECOVER_FROM_CHARGE;
        enemy = _enemy;
        
    }

    public override void Destroy()
    {

        enemy = null;

    }

    public override void Enter()
    {

        enemy.agent.enabled = false;
        
        enemy.animator.SetBool("Walking", false);
        enemy.animator.SetTrigger("TakeDamage1");
        //enemy.GoTo(enemy.transform.position + enemy.knockbackDirection);
        enemy.rbody.isKinematic = false;
        enemy.rbody.useGravity = true;


        enemy.unconciousTimer = enemy.recoveryStandingTime;
        


    }

    public override void Exit()
    {
        enemy.agent.enabled = true;
        enemy.rbody.isKinematic = true;
        enemy.rbody.useGravity = false;

        Destroy();
    }

    public override void Update()
    {

        enemy.rbody.velocity = enemy.knockbackDirection;

        if (enemy.unconciousTimer <= 0)
            enemy.changeState(new EnemyIdle(enemy));
    }
}