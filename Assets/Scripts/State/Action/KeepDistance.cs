﻿using UnityEngine;
using System.Collections;

public class KeepDistance : ActionBase
{

    private EnemyAI enemy;
    private Transform target;

    float checkInterval = 0.2f;
    float currentCheckTime = 0;


    public KeepDistance(EnemyAI _enemy,Transform _target)
    {
        type = ActionType.KEEP_DISTANCE;
        enemy = _enemy;
        target = _target;

    }

    public override void Destroy()
    {
        target = null;
        enemy = null;

    }

    public override void Enter()
    {
        Debug.Log("KEEP DISTANS");
        enemy.agent.isStopped = false;
        enemy.animator.SetBool("Walking", true);        

    }

    public override void Exit()
    {

        
        Destroy();
    }

    public override void Update()
    {
        if (!enemy.playerInAggroRadius)
        {
            enemy.changeState(new EnemyIdle(enemy));
            return;
        }
        

        if ( enemy.unconciousTimer > 0)
            return;

        if (enemy.agent.destination.x < enemy.transform.position.x)
        {
            enemy.transform.localScale = new Vector3(-1, 1, 1);
        }
        else enemy.transform.localScale = new Vector3(1, 1, 1);

        currentCheckTime += Time.deltaTime;
        if( currentCheckTime > checkInterval) { 
            enemy.GoTo(target.transform.position - (target.transform.position - enemy.transform.position).normalized * enemy.attackRange*3f,false);
            currentCheckTime = 0;
        }

        if (BeatemupMaths.GetFakeDistanceBetween(enemy.transform.position, target.position) >= enemy.attackRange *0.8f)
        {
            enemy.changeState(new EnemyIdle(enemy));
        }



    }
}