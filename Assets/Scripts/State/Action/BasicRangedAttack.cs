﻿using UnityEngine;
using System.Collections;

public class BasicRangedAttack : ActionBase
{

    private EnemyAI enemy;
    private Transform target;
    Coroutine attackRoutine;

    public BasicRangedAttack(EnemyAI _enemy, Transform _target)
    {
        type = ActionType.BASIC_RANGED_ATTACK;
        enemy = _enemy;
        target = _target;

    }

    IEnumerator CreateProjectile()
    {
        yield return new WaitForSeconds(enemy.projectileAnimationTime);

        if (enemy.unconciousTimer > 0)
            yield return 0;

        GameObject tempProjectileObj = ProjectileManager.Instance.getProjectile(enemy.projectileType);

        Projectile tempProjectile = tempProjectileObj.GetComponent<Projectile>();


        tempProjectile.direction = target.position - enemy.transform.position;
        tempProjectileObj.transform.position = enemy.transform.position + enemy.projectileSpawnOffset;
        tempProjectile.Fire();

    }


    public override void Destroy()
    {

        enemy = null;

    }

    public override void Enter()
    {


        enemy.agent.isStopped = true;
        enemy.animator.SetBool("Walking", false);
        enemy.curAttackingTime = enemy.attackTime;
        
        enemy.animator.SetTrigger("Punch1");
        attackRoutine = enemy.StartCoroutine(CreateProjectile());


    }

    public override void Exit()
    {
        enemy.StopCoroutine(attackRoutine);
        enemy.agent.isStopped = false;
        Destroy();
    }

    public override void Update()
    {
        if( enemy.curAttackingTime <= 0.3f && enemy.unconciousTimer <= 0)
        {
            
            enemy.changeState(new EnemyIdle(enemy));

        }
    }
}