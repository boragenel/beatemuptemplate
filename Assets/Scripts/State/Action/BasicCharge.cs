﻿using UnityEngine;
using System.Collections;

public class BasicCharge : ActionBase
{

    private EnemyAI enemy;
    private Transform target;
    Coroutine prepareRoutine;
    float range = 10f;
    float time = 1f;

    Vector3 targetPos;

    float defaultSpeed = 0;
    float defaultAcceleration = 0;

    Vector3 attackVector;

    public BasicCharge(EnemyAI _enemy, Transform _target, float _range = 10f)
    {
        type = ActionType.BASIC_CHARGE;
        enemy = _enemy;
        target = _target;
        range = _range;

    }




    public override void Destroy()
    {

        enemy = null;

    }

    public override void Enter()
    {
        enemy.agent.isStopped = false;
        enemy.animator.SetBool("Walking", true);

        defaultSpeed = enemy.agent.speed;
        defaultAcceleration = enemy.agent.acceleration;
        enemy.agent.speed = 8;
        enemy.agent.acceleration = 14f;
        attackVector = (target.position - enemy.transform.position).normalized;
        attackVector.y = 0;
        enemy.agentControlZ = true;

        enemy.damageSphere2.SetActive(true);
        

        targetPos = enemy.transform.position + (attackVector * 100f);

        enemy.agent.enabled = false;
        //enemy.GoTo(enemy.transform.position + ((attackVector * (range + (range* Mathf.Abs(attackVector.z / attackVector.x) )))));
        

    }

    public override void Exit()
    {

        enemy.damageSphere2.SetActive(false);
        enemy.agentControlZ = false;
        enemy.agent.speed = defaultSpeed;
        enemy.agent.acceleration = defaultAcceleration;
        enemy.agent.enabled = true;
        enemy.agent.isStopped = false;
        Destroy();
    }

    public override void Update()
    {
        time -= Time.deltaTime;

        
        enemy.transform.position += attackVector *  ((enemy.agent.speed * 1.5f) ) * Time.deltaTime;

        
        


        if (BeatemupMaths.GetFakeDistanceBetween(enemy.transform.position, targetPos) < 1.4f || time <= 0)
        {
            enemy.changeState(new RecoverFromCharge(enemy));
        }
    }

    
}