﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public enum EnemyStyle
{
    CHASE,
    GUARD,
    KEEPDISTANCE,
    GUARD_KEEPDISTANCE

}

public enum AttackStyle
{
    MELEE_ATTACK,
    RANGED_ATTACK,
    MELEE_SUICIDE,
    CHARGE

}

public enum EnemyState
{
    CHASING,
    ATTACKING,
    RETURN_TO_GUARD,
    IDLE,
    KEEPING_DISTANCE,
    PATROLLING,
    PREPARE_CHARGE,
    CHARGING
}

public class EnemyAI : BStateMachine {

    public NavMeshAgent agent;
    public Transform player;
    public Animator animator;
    public Rigidbody rbody;
    public GameObject charHolder;

    public bool agentControlZ = false;

    [Header("Style")]
    public EnemyStyle enemyStyle;
    public AttackStyle attackStyle;
    public ProjectileType projectileType = ProjectileType.FIRE;

    // If guarding a point, returns to this point when the player is out of the guarding radius
    public Vector3 guardingPos;
    // When guarding a point, when the player leaves this radius, the enemy stop chasing
    public float guardingRadius = 10f;

    public bool returningToGuardPos = false;

    public float playerSeekTimer = 2f;

    public Vector3 knockbackDirection = Vector3.zero;



    [Header("Offense")]

    public bool playerInAggroRadius = false;
    public bool playerInAttackRadius = false;



    private bool attacking = false;
    // How far the enemy can sight the player
    public float aggroRadius = 7f;

    public float attackZRange = 0.4f;
    public float attackRange = 0.8f;

    public float attackTime = 0.4f;
    public float curAttackingTime = 0f;

    public float knockbackPower = 0.5f;

    public float projectileAnimationTime = 0.3f;
    public Vector3 projectileSpawnOffset = Vector3.zero;

    public GameObject damageSphere;
    public GameObject damageSphere2;

    


    [Header("Defence")]

    

    // between 0 - 1
    public float damageReduction = 0f;

    // between 0 - 1
    public float knockbackReduction = 0.1f;

    // between 0 - 1
    public float blockChance = 0.15f;

    // unconcious time after getting hit
    public float recoveryStandingTime = 0.2f;
    // unconcious time when knocked down
    public float recoveryGroundTime = 1.3f;

    public bool gotGrappled = false;

    [Header("Stats")]
    
    public float unconciousTimer = 0;


    // Use this for initialization
    void Start () {
        agent = GetComponent<NavMeshAgent>();
        guardingPos = transform.position;
        animator = GetComponent<Animator>();
        rbody = GetComponent<Rigidbody>();

        changeState(new EnemyIdle(this));

    }
	

    public void GoTo(Vector3 _pos,bool playerChase=true)
    {
        if (!agent.enabled)
            return;

        if( playerChase && currentState.type != ActionType.BASIC_CHARGE )
            agent.destination = _pos + ((transform.position - _pos).normalized * attackRange*0.95f);
        else
            agent.destination = _pos + ((transform.position - _pos).normalized *0.95f);

        // TODO: could be something else for a different type of enemy
        if (!animator.GetBool("Walking") && unconciousTimer <= 0)
            animator.SetBool("Walking", true);
    }


    void HandleKnockback()
    {

        knockbackDirection *= 0.9f;
        if( knockbackDirection.magnitude < 0.3f )
        {
            knockbackDirection = Vector3.zero;
        }

    }

	// Update is called once per frame
	void Update () {

        

        HandleTimers();
        HandleKnockback();
        

        if (playerSeekTimer <= 0)
            playerInAggroRadius = CheckIfPlayerIsAround();

        if (playerInAggroRadius && unconciousTimer <= 0) {

            if (currentState.type == ActionType.IDLE )
                changeState(new ChaseObject(this, player));

            
            AttackCheck();

        }

        UpdateState();

        if(!agentControlZ)
        { 
            Vector3 newV = agent.desiredVelocity;
            //newV.z *= 1;
            newV += knockbackDirection;

            agent.velocity = newV;

        }

    }


    public void HandleTimers()
    {

        if (playerSeekTimer > 0)
            playerSeekTimer -= Time.deltaTime;

        if (unconciousTimer > 0)
            unconciousTimer -= Time.deltaTime;

        if (curAttackingTime > 0)
        {
            curAttackingTime -= Time.deltaTime;
        }


        if(curAttackingTime <= 0 && unconciousTimer <= 0)
        {
            agent.enabled = true;
        }
        
    }


    public bool CheckIfPlayerIsAround()
    {

        Vector3 playerPos = player.position;
        float dist = BeatemupMaths.GetFakeDistanceBetween(playerPos, transform.position);

        if (dist > aggroRadius)
        {
            playerSeekTimer = dist / 30f;
            return false;
        }
        else
            return true;

    }

    


    private void AttackCheck()
    {

        if (unconciousTimer > 0 || curAttackingTime > 0 || 
            currentState.type == ActionType.KEEP_DISTANCE ||
            currentState.type == ActionType.GUARD )
            return;

        

        if (BeatemupMaths.GetFakeDistanceBetween(player.position, transform.position) < attackRange)
        {
            playerInAttackRadius = true;
            animator.SetBool("Walking", false);

            if (player.transform.position.x < transform.position.x)
            {
                transform.localScale = new Vector3(-1, 1, 1);
            }
            else transform.localScale = new Vector3(1, 1, 1);


            switch (attackStyle)
            {
                case AttackStyle.MELEE_ATTACK:
                    rbody.velocity = Vector3.zero;
                    changeState(new BasicMeleeAttack(this));
                    break;
                case AttackStyle.RANGED_ATTACK:
                    rbody.velocity = Vector3.zero;
                    changeState(new BasicRangedAttack(this,player));
                   
                    break;

                case AttackStyle.CHARGE:
                    rbody.velocity = Vector3.zero;
                    changeState(new PrepareForCharge(this,player,attackRange));
                    break;

                case AttackStyle.MELEE_SUICIDE:
                    rbody.velocity = Vector3.zero;
                    changeState(new PrepareForSuicide(this, player));
                    break;

            }

        }
        else playerInAttackRadius = false;

    }


    private void OnTriggerEnter(Collider other)
    {
        if( other.tag == "PlayerDamage" )
        {

            EffectsController.Instance.hitImpact1.transform.position = other.transform.position + (Vector3.up * Random.Range(-0.1f,0.1f));
            EffectsController.Instance.hitImpact1.Play();

            float power = other.GetComponentInParent<BeatemUpPlayerController>().knockbackPower;
            knockbackDirection = (transform.position - player.transform.position).normalized * power *4f;
            knockbackDirection.y = 0;
            if( !gotGrappled )
                changeState(new TakeDamage(this));
            
        }
    }


}
